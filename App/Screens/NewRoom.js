import React, {Component} from "react"
import {FlatList, Image, ScrollView, Text, TouchableOpacity, View} from "react-native"
import {connect} from "react-redux"
import * as Qiscus from "../Services/qiscus"

class NewRoom extends Component {
    constructor(props) {
        super(props)
        this.state = {
            contacts: []
        }
    }

    getUser = query => {

        Qiscus.qiscus.getUsers(query)
            .then(data => {
                this.setState({contacts: data.users})
            })
            .catch({})

    }

    componentDidMount() {
        this.subscription = Qiscus.isLogin$()
            .filter(isLogin => isLogin === true)
            .subscribe({
                next: async () => {
                    this.exitRoom()
                    this.getUser("")
                }
            })
    }

    componentWillUnmount() {
        this.exitRoom()
    }

    exitRoom = () => {
        this.subscription && this.subscription.unsubscribe()
    }

    createRoom = email =>{
        Qiscus.qiscus.chatTarget(email)
            .then(data =>{
                this.props.navigation.navigate("Chat", {item: data})
            })
            .catch(error=>{
            })
    }
    render() {
        let data = this.state.contacts
        console.log(data)
        return (
            <View style={{flex: 1}}>
                <View style={{
                    flexDirection: "row", alignItems: "center", backgroundColor: "#eb180f", padding: 10,
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 1,
                    },
                    shadowOpacity: 0.22,
                    shadowRadius: 2.22,
                    elevation: 3,
                }}>
                    <TouchableOpacity style={{alignItems: "center"}} onPress={() => {
                        this.exitRoom()
                        this.props.navigation.replace("Home")
                    }}>
                        <Image source={require("../Assets/arrow_back_white.png")}
                               style={{width: 25, height: 25, marginRight: 20}}/>
                    </TouchableOpacity>
                    <View style={{padding: 6}}>
                        <Text style={{fontWeight: "bold", color: "#ffffff", fontSize: 20}}>Choose Contacts</Text>
                    </View>
                </View>
                <ScrollView>
                    <View style={{padding: 5}}>
                        {/*<TextInput placeholder={"Search"} style={{backgroundColor: "#ffffff"}}/>*/}
                        {/*<Text>CONTACTS</Text>*/}
                        <FlatList
                            data={Object.keys(data)}
                            keyExtractor={(item, index) => index.toString()}
                            renderItem={({item, index}) => {
                                return (
                                    <TouchableOpacity onPress={() => this.createRoom(data[item].email)}>
                                        <View style={{backgroundColor: "#ffffff"}}>
                                            <View style={{flexDirection: "row", alignItems: "center", padding: 10}}>
                                                <Image source={{uri: data[item].avatar_url}} style={{
                                                    height: 25,
                                                    width: 25,
                                                    marginRight: 10
                                                }}/>
                                                <Text>{data[item].name}</Text>
                                            </View>
                                            <View style={{height: 2, backgroundColor: "#eeeeee"}}/>
                                        </View>
                                    </TouchableOpacity>
                                )
                            }}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const mapsStateToProps = (state) => {
    return {
        data: state.data
    }
}
export default connect(mapsStateToProps)(NewRoom)
const style = {};
