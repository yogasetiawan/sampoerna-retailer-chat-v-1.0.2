import React, {Component} from "react";
import {
    Alert,
    FlatList,
    Image,
    RefreshControl,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from "react-native";
import * as Qiscus from "../Services/qiscus";
import xs from "xstream";
import moment from "moment"
import {
    updateRoom,
    setDataUser,
    setLastUpdateRoom,
    setRooms,
    setMessages,
    deletedMessage
} from "../Services/Redux/action/Data";
import {connect} from "react-redux";
import firebase from 'react-native-firebase';

class Home extends Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props)
        this.state = {
            login: false,
            isFetching: false,
            lastNotif: null,

        }
        this.navigateChat = this.navigateChat.bind(this)
    }

    componentDidMount() {
        console.log("componentDidMount Home")
        console.disableYellowBox = true;
        this.fetchMsg(true)
        this.subscription2 =
            xs.merge(
                Qiscus.newMessage$().map(this._onNewMessage),
                Qiscus.commentDeleted$().map(this._onDeletedComment)
            ).subscribe({
                next: () => {
                },
                error: error => console.log("subscription error", error)
            })
        this.firebase()
    }

    firebase = () => {
        //handle background applications
        firebase.notifications().onNotificationOpened(({notification}) => {
            firebase.notifications().removeDeliveredNotification(notification.notificationId)
            let item = JSON.parse(notification.data.payload)
            this.navigateChat(item, false)
        })

        //handle foreground applications
        firebase.notifications().getInitialNotification()
            .then(payload => {
                firebase.notifications().removeDeliveredNotification(payload.notificationId)
                let {notification} = payload
                if (notification) {
                    let item = JSON.parse(notification.data.payload)
                    this.state.item !== item && this.navigateChat(item, false)
                    this.setState({lastNotif: item})
                }
            })
    }
    fetchMsg = (refresh = true) => {
        refresh && this.setState({isFetching: true, lastUpdate: moment(new Date()).unix()})

        this.subscription = Qiscus.isLogin$()
            .filter(isLogin => isLogin === true)
            .take(1)
            .map(() => xs.from(Qiscus.qiscus.loadRoomList()
                .then(data => {
                    return data
                })
                .catch(error => {
                    setTimeout(() => {
                        //ToastAndroid.show("Gagal mengambil data dari server", ToastAndroid.SHORT)
                        this.fetchMsg(false)
                    }, 5000)
                    this.setState({isFetching: false})
                })
            ))
            .flatten()
            .subscribe({
                next: rooms => {
                    if (rooms) {
                        let data = rooms.reduce((result, room) => {
                            result[room.id] = room
                            return result
                        }, {})
                        this.props.dispatch(setLastUpdateRoom(moment(new Date()).unix()))
                        this.props.dispatch(setRooms(data))

                        this.setState({
                            isFetching: false
                        })
                    }

                }
            });
    }
    _onNewMessage = data => {
        console.log("_onNewMessage Home", data)
        if (data.unix_timestamp > this.props.data.lastUpdateRoom) {
            if (this.props.data.rooms[data.room_id] == undefined) {
                Qiscus.qiscus.getRoomById(data.room_id)
                    .then(room => {
                        this.props.dispatch(updateRoom(data.room_id, room))
                    })
                    .catch({})
            }

            this._onNewMessage$ =
                xs.from(Object.values(this.props.data.rooms))
                    .filter(it => it.id === data.room_id)
                    .subscribe({
                        next: it => {
                            this.fetchMsg(false)
                        }
                    })
        }
    }
    _onDeletedComment = data => {
        this.props.dispatch(deletedMessage(data.roomId, data.commentUniqueIds))
    }

    componentWillUnmount() {
        console.disableYellowBox = false;
        this.onUnmount()
    }

    onUnmount = () => {
        console.log("onUnmount")
        this.subscription && this.subscription.unsubscribe();
        this.subscription2 && this.subscription2.unsubscribe();
        this._onNewMessage$ && this._onNewMessage$.unsubscribe();
        this.deleteMessage$ && this.deleteMessage$.unsubscribe();
    }

    navigateChat = (data, update = true) => {
        let dataQismo = {
            'email': this.props.data.account.username + "@mail.com",
            'username': this.props.data.account.username
        }
        let {room_options} = data
        const roomId = data.room_id || data.id
        const state = Object.values(this.props.data.rooms)
        let filter = state.filter(it => it.id === roomId)
        if (filter.length > 0) {
            filter[0].count_notif = 0;
            update !== false && this.props.dispatch(updateRoom(roomId, data))
            this.props.navigation.navigate("Chat", {item: data})
        } else {
            if (JSON.parse(room_options).app_id) {
                this.props.navigation.navigate("Qismo", dataQismo)
            } else {
                this.props.navigation.navigate("Chat", {item: data})
            }
        }
    }

    deleteMessage = item => {
        console.log("deleteMessage")
        let rooms_ = Object.values(this.props.data.rooms)
        Alert.alert("Alert", "Delete Room?",
            [
                {text: "Cancel", style: "cancel"},
                {
                    text: "Ok", onPress: () => {
                        Qiscus.qiscus.clearRoomMessages([item.unique_id])
                            .then(data => {
                                let rooms = data.results.rooms
                                this.deleteMessage$ = xs.from(rooms)
                                    .subscribe({
                                        next: it => {
                                            let _data = rooms_.filter(a => a.id !== it.id);
                                            let data = _data.reduce((result, room) => {
                                                result[room.id] = room
                                                return result
                                            }, {})
                                            this.props.dispatch(setRooms(data))
                                            this.props.dispatch(setMessages(it.id, {}))
                                        }
                                    })
                            })
                            .catch(error => {
                                console.log("delete gagal ", error)
                            })
                    }
                }
            ])
    }

    render() {
        let dataQismo = {
            'email': this.props.data.account.username + "@mail.com",
            'username': this.props.data.account.username
        }

        let data = Object.values(this.props.data.rooms)
        let _data = data.length > 0 ? data : []
        _data.sort((a, b) => b.last_comment_id - a.last_comment_id)

        return (
            <View style={{flex: 1}}>
                <View style={{
                    flexDirection: "row", alignItems: "center", backgroundColor: "#eb180f", padding: 10,
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 1,
                    },
                    shadowOpacity: 0.22,
                    shadowRadius: 2.22,
                    elevation: 3,
                }}>
                    <View style={{marginLeft: 10, padding: 6}}>
                        <Text style={{fontWeight: "bold", color: "#ffffff", fontSize: 20}}>Home</Text>
                    </View>
                    <View style={{position: "absolute", right: 10}}>
                        <TouchableOpacity onPress={() => {
                            this.props.dispatch(setDataUser([]))
                            this.props.dispatch(setRooms([]))
                            this.onUnmount()
                            this.setState({login: false})
                            this.props.navigation.replace("Login")
                        }}>

                            <Text style={{fontWeight: "bold", color: "#ffffff", fontSize: 20}}>Keluar</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{
                    position: 'absolute',
                    zIndex: 1,
                    bottom: 20,
                    right: 20
                }}>
                    <View style={{
                        width: 80,
                        height: 80,
                        borderRadius: 100,
                        backgroundColor: '#66bb6a'
                    }}>
                        <TouchableOpacity
                            style={{
                                alignItems: 'center', paddingTop: 15
                            }}
                            onPress={() => this.props.navigation.navigate("NewRoom")}
                        >
                            <Image source={require('../Assets/chat-white.png')} style={{width: 25, height: 25}}/>
                            <Text style={{color: 'white', fontWeight: 'bold', fontSize: 12, marginTop: 5}}>CHAT
                                USER</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{
                        width: 80,
                        height: 80,
                        marginTop: 10,
                        borderRadius: 100,
                        backgroundColor: '#66bb6a'
                    }}>
                        <TouchableOpacity
                            style={{
                                alignItems: 'center', paddingTop: 15
                            }}
                            onPress={() => this.props.navigation.navigate("Qismo", dataQismo)}
                        >
                            <Image source={require('../Assets/chat-white.png')} style={{width: 25, height: 25}}/>
                            <Text style={{color: 'white', fontWeight: 'bold', fontSize: 12, marginTop: 5}}>CHAT
                                AGEN</Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <FlatList
                    data={_data}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({item, index}) => {
                        return (
                            <TouchableOpacity
                                onLongPress={() => this.deleteMessage(item)}
                                onPress={() => this.navigateChat(item)}>
                                <View style={{
                                    width: "100%",
                                    height: 60,
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    padding: 10
                                }}>
                                    <Text style={{
                                        position: 'absolute',
                                        top: 1,
                                        right: 1,
                                        padding: 10
                                    }}>{moment(item.last_comment_message_created_at).format('MM/DD/YY')}</Text>
                                    {item.count_notif > 0 && <Text
                                        style={{
                                            position: 'absolute',
                                            top: 30,
                                            right: 20,
                                            padding: 5,
                                            color: "#eee",
                                            fontSize: 10,
                                            borderRadius: 100,
                                            backgroundColor: "#5EB8FF"
                                        }}>{item.count_notif}</Text>}
                                    <Image source={{uri: item.avatar}}
                                           style={{height: 40, width: 40, borderRadius: 100}}/>
                                    <View style={{marginLeft: 10}}>
                                        <Text style={{fontWeight: 'bold'}}>{item.name}</Text>
                                        <Text numberOfLines={1} style={{
                                            color: "#a3a3a3",
                                            width: 200
                                        }}>{item.last_comment_message}</Text>
                                    </View>
                                </View>
                                <View style={{backgroundColor: "#E8EAF6", height: 1}}/>
                            </TouchableOpacity>
                        )
                    }}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.isFetching}
                            onRefresh={() => this.fetchMsg()}
                        />
                    }
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    rightAction: {
        backgroundColor: '#dd2c00',
        justifyContent: 'center',
        flex: 1,
        alignItems: 'flex-end',
    },
    actionText: {
        color: '#fff',
        fontWeight: '600',
        padding: 20,
        fontSize: 15
    },
})

const mapsStateToProps = (state) => {
    return {
        data: state.data
    }
}
export default connect(mapsStateToProps)(Home)
