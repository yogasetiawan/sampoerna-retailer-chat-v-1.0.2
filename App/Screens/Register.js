import React, {Component} from "react"
import {ActivityIndicator, Image, Modal, ScrollView, Text, TextInput, TouchableOpacity, View} from "react-native"
import * as Qiscus from "../Services/qiscus"
import {connect} from "react-redux"
import {setDataUser} from "../Services/Redux/action/Data";

class Register extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: '',
            pass: '',
            name: '',
            loading: false
        }
    }

    onSubmit = () => {
        this.setState({loading: true})
        Qiscus.qiscus.setUser(this.state.user, this.state.pass, this.state.name)
            .catch(() => console.log("Failed login"));
    }

    componentDidMount() {
        this.subscription = Qiscus.login$()
            .map(data => data.user)
            .take(1)
            .subscribe({
                next: data => {
                    this.setState({loading: false})
                    this.props.navigation.replace("Home")
                    this.props.dispatch(setDataUser(data))
                }
            });
    }

    componentWillUnmount() {
        this.subscription && this.subscription.unsubscribe()
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <ScrollView>
                    <Modal
                        animationType="none"
                        visible={this.state.loading}
                        style={{flex: 1}}
                        transparent={true}
                        onRequestClose={() => this.setState({loading: false})}
                    >
                        <ActivityIndicator
                            size="large"
                            color="#eb180f"
                            style={{
                                position: "absolute",
                                top: 0,
                                bottom: 0,
                                right: 0,
                                left: 0
                            }}/>
                    </Modal>
                    <View style={style.logo}>
                        <Image source={require('../Assets/logo.png')} style={{height: 100, width: 100}}/>
                    </View>
                    <View style={style.body}>
                        <Text style={style.text}>USER ID</Text>
                        <TextInput
                            placeholder={"Input user id"}
                            style={style.textInput}
                            onChangeText={(user) => this.setState({user})}
                            defaultValue={this.state.user}
                        />
                        <Text style={style.text}>DISPLAY NAME</Text>
                        <TextInput
                            placeholder={"Input Display Name"}
                            style={style.textInput}
                            defaultValue={this.state.name}
                            onChangeText={(name) => this.setState({name})}/>
                        <Text style={style.text}>USER KEY</Text>
                        <TextInput
                            placeholder={"Input user key"}
                            style={style.textInput}
                            defaultValue={this.state.pass}
                            secureTextEntry={true}
                            onChangeText={(pass) => this.setState({pass})}/>
                        <TouchableOpacity onPress={() => this.onSubmit()}>
                            <View style={style.btn}>
                                <Text style={style.btnText}>Register</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const mapsStateToProps = (state) => {
    return {
        data: state.data
    }
}
export default connect(mapsStateToProps)(Register)
const style = {
    logo: {width: "100%", alignItems: 'center', marginTop: 100},
    body: {marginTop: 30, paddingLeft: 50, paddingRight: 50},
    text: {fontWeight: 'bold', marginTop: 10, color: '#979797'},
    textInput: {borderBottomWidth: 1, borderBottomColor: '#eb180f', paddingLeft: 15},
    btn: {
        backgroundColor: '#dc1e2d',
        padding: 10,
        borderRadius: 5,
        marginTop: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnText: {color: 'white', fontWeight: 'bold', fontSize: 20},
    btnIcon: {width: 25, height: 25}
};
