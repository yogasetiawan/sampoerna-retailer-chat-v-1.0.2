import React, {Component} from "react";
import {WebView} from "react-native-webview";
import axios from "axios";
import firebase from "react-native-firebase";

class Qismo extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            "token": ''
        }
    }

    componentDidMount() {
        firebase.messaging().getToken().then(token=>{
            this.setState({token})
            console.log(token)
        });
    }

    onMessage = async (event) => {
        let data = null
        try {
            data = JSON.parse(event.nativeEvent.data)
        } catch (e) {
            data = event.nativeEvent.data
        }
        console.log(data)
        if (data === "exit") {
            this.props.navigation.goBack()
        } else if (data.AppId) {
            axios
                .post(
                    `https://api.qiscus.com/api/v2/mobile/set_user_device_token`,
                    {
                        token: data.UserToken,
                        device_token: this.state.token,
                        device_platform: "rn"
                    },
                    {
                        headers: {
                            qiscus_sdk_app_id: data.AppId,
                            qiscus_sdk_token: data.UserToken,
                            qiscus_sdk_user_id: data.UserId
                        }
                    }
                )
                .then(res => {
                    console.log("success sending device token", res);
                })
                .catch(error => {
                    console.log("failed sending device token", error);
                });
        }
    }

    render() {
        let {username, email} = this.props.navigation.state.params
        const sourceUri = (Platform.OS === 'android' ? 'file:///android_asset/' : '') + `Web.bundle/qismo/index.html?email=${email}&username=${username}`;
        return (
            <WebView
                source={{uri: sourceUri}}
                style={{flex:1}}
                startInLoadingState={false}
                javaScriptEnabled={true}
                originWhitelist={['*']}
                allowFileAccess={true}
                onMessage={this.onMessage}
            />
        )
    }
}

export default Qismo