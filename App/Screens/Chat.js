import React, {Component} from "react";
import {ActivityIndicator, Alert, FlatList, Image, Text, TextInput, TouchableOpacity, View} from "react-native";
import xs from 'xstream'
import * as Qiscus from '../Services/qiscus'
import moment from "moment";
import {connect} from "react-redux";
import _ from "lodash"
import ImagePicker from 'react-native-image-picker'
import {setCurretRoom, setMessages, updateMessage, updateMessages, updateRoom} from "../Services/Redux/action/Data";

class Chat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: '',
            isLoadMoreable: true,
            message: '',
            online: [],
            isTyping: false,
            isFetching: false,
            onFocusText: false
        }
        this.userTypingDelay = _.throttle(this.userTyping, 2000)
    }

    static navigationOptions = {
        header: null
    };

    async componentDidMount() {
        this.setState({isFetching: this.messages().length <= 0})

        let props = this.props.navigation.getParam("item")
        const roomId = props.room_id || props.id
        this.subscription1 = await Qiscus.isLogin$()
            .take(1)
            .map(() => xs.from(Qiscus.qiscus.getRoomById(roomId)))
            .flatten()
            .subscribe({
                next: room => {
                    this.props.dispatch(setCurretRoom(room))
                }
            })

        this.subscription2 = await Qiscus.isLogin$()
            .take(1)
            .map(() => xs.from(Qiscus.qiscus.loadComments(roomId)))
            .flatten()
            .subscribe({
                next: async messages => {
                    const message_ = await messages.reverse()
                    const formattedMessages = await message_.reduce((result, message) => {
                        result[message.unique_temp_id] = message;
                        return result;
                    }, {});
                    await this.setState({
                        messages: formattedMessages
                    })
                    await this.props.dispatch(setMessages(roomId, formattedMessages))
                    await this.setState({isFetching: false})
                }
            });

        this.subscription3 = xs
            .merge(
                Qiscus.newMessage$().map(this._onNewMessage),
                Qiscus.onlinePresence$().map(this._onOnline),
                Qiscus.messageDelivered$().map(this._messageDelivered),
                Qiscus.messageRead$().map(this._messageRead),
                Qiscus.typing$().map(this._onTyping),
            )
            .subscribe({
                next: () => {
                },
                error: error => console.log("subscription error", error)
            });
    }

    _onOnline = data => {
        //console.log("_onOnline : ", data)
        //  data && this.setState({online: data})
        return "online"
    }
    _onNewMessage = message => {
        console.log("_onNewMessage ", message)
        this.props.data.curretRoom.id == message.room_id && this.props.dispatch(
            updateMessage(this.getRoomId(), message.unique_temp_id, message)
        );

        return "New message"
    }
    _onTyping = data => {
        console.log("_onTyping", data)
        if (this.props.data.curretRoom.id == data.room_id) {
            console.log("_onTyping 2 ", data)
            if (!this.state.isTyping && data.message === '1') {
                this.setState({isTyping: true})
                setTimeout(() => {
                    this.setState({isTyping: false})
                }, 10000)
            } else {
                this.setState({isTyping: false})
            }
        }
    }

    _messageDelivered = data => {
        console.log("On messageDelivered : ", data)
        let {comment} = data
        let msg = this.messages()
            .filter(it => it.timestamp <= comment.timestamp && it.status !== "read")
            .map(it => ({...it, status: "delivered"}))

        let messages = msg.reduce((result, item) => {
            let uniqueId = item.unique_id || item.unique_temp_id
            result[uniqueId] = item
            return result
        }, {})
        Qiscus.qiscus.readComment(this.getRoomId(), comment.id)
        this.props.dispatch(updateMessages(this.getRoomId(), messages))

        return "Message delivered"
    }
    _messageRead = ({comment}) => {
        console.log("On messageRead 1 : ", comment)
        let results = this.messages()
            .filter(it => it.timestamp <= comment.timestamp)
            .map(it => ({...it, status: "read"}));

        let messages = results.reduce((result, item) => {
            const uniqueId = item.unique_id || item.unique_temp_id;
            result[uniqueId] = item;
            return result;
        }, {});

        this.props.dispatch(updateMessages(this.getRoomId(), messages))
    }

    sendMessage = message => {
        let now = moment.utc()
        let _msg = {
            "unique_temp_id": now.unix(),
            "time": now,
            "timestamp": now,
            "type": "text",
            "email": this.props.data.account.email || "",
            "status": "pending",
            "message": message
        }

        this.props.dispatch(updateMessage(this.getRoomId(), now.unix(), _msg))

        this.setState({message: ''})

        Qiscus.qiscus.sendComment(this.getRoomId(), message, now.unix())
            .then(data => {
                console.log("send message : ", message)
                this.props.dispatch(updateMessage(this.getRoomId(), data.unique_temp_id, data))
            })
            .catch(error => {
                _msg.status = "failed"
                this.props.dispatch(updateMessage(this.getRoomId(), now.unix(), _msg))
                Alert.alert("Error", "Failed to send message")
            })
    }
    componentWillUnmount() {
        this.exitRoom()
    }
    getRoomId = () => {
        let props = this.props.navigation.getParam("item")
        const roomId = props.room_id || props.id
        return roomId
    }
    getOnline() {
        let data = this.state.online;
        if (data.length > 0) {
            let online = data.split(":")
            if (online[0] == 1) {
                return "online"
            } else {
                return "offline"
            }
        }
        return "offline"
    }
    resendMsg = msg => {
        msg.status = "failed"
        let roomId = this.getRoomId()
        let {message, unique_temp_id, type} = msg
        let payload = JSON.stringify(msg.payload) || ""
        this.props.dispatch(updateMessage(this.getRoomId(), msg.unique_temp_id, msg))

        Qiscus.qiscus.sendComment(roomId, message, unique_temp_id, type, payload)
            .then(data => {
                this.props.dispatch(updateMessage(roomId, data.unique_temp_id, data))
            })
            .catch(error => {
                Alert.alert("Error", "Failed to send message")
                msg.status = "failed"
                this.props.dispatch(updateMessage(this.getRoomId(), msg.unique_temp_id, msg))
            })
    }
    statusMsg = status => {
        const send = <Image source={require("../Assets/msg_status_send.png")} style={{width: 20, height: 20}}/>
        const delivered = <Image source={require("../Assets/msg_status_delivered.png")}
                                 style={{width: 20, height: 20}}/>
        const read = <Image source={require("../Assets/msg_status_read.png")} style={{width: 20, height: 20}}/>
        const pending = <Image source={require("../Assets/msg_status_pending.png")} style={{width: 20, height: 20}}/>
        const failed = <Image source={require("../Assets/msg_status_failed.png")} style={{width: 20, height: 20}}/>

        if (status === "sent") {
            return send
        } else if (status === "read") {
            return read
        } else if (status === "delivered") {
            return delivered
        } else if (status === "failed") {
            return failed
        } else {
            return pending
        }
    }
    userTyping = val => {
        Qiscus.qiscus.publishTyping(val)
    }
    deleteMsg = item => {
        let message_ = this.messages()
        Alert.alert("Alert", "Delete Message ?",
            [
                {text: "Cancel", style: "cancel"},
                {
                    text: "Ok", onPress: () => {
                        Qiscus.qiscus.deleteComment(item.room_id, [item.unique_temp_id])
                            .then(() => {

                                const msg = message_.filter(it => it.id !== item.id)

                                let messages = msg.reduce((result, item) => {
                                    let uniqueId = item.unique_id || item.unique_temp_id
                                    result[uniqueId] = item
                                    return result
                                }, {})

                                this.props.dispatch(setMessages(this.getRoomId(), messages))
                            })
                            .catch(error => {
                                console.log("error", error)
                            })
                    }
                }
            ])

    }
    exitRoom = () => {
        this.props.dispatch(setCurretRoom({}))
        Qiscus.qiscus.exitChatRoom();
        this.subscription1 && this.subscription1.unsubscribe()
        this.subscription2 && this.subscription2.unsubscribe()
        this.subscription3 && this.subscription3.unsubscribe()

        this.setState({
            userId: '',
            messages: {},
            room: null,
            isLoadMoreable: true,
            message: '',
            online: [],
            isTyping: false,
            isFetching: false,
            onFocusText: false
        })

    }
    loadMoreMsg = () => {
        let state = this.messages()
        let last_comment = state.slice(0).pop()
        let options = {
            last_comment_id: last_comment.id,
            limit: 10
        }
        Qiscus.qiscus.loadComments(last_comment.room_id, options)
            .then(it => {
                let data = it.reverse()
                let msg = data.reduce((result, message) => {
                    result[message.unique_temp_id] = message;
                    return result;
                }, {})

                this.props.dispatch(updateMessages(last_comment.room_id, msg))
            })
            .catch({})
    }
    cekTyping = () => {
        if (this.state.onFocusText || this.state.message !== '') {
            return true
        } else {
            return false
        }
    }
    onChoosePhoto = () => {
        const options = {
            noData: true
        }

        ImagePicker.launchImageLibrary(options, resp => {
            if (resp.uri) {
                this.setState({isFetching: true})
                const opts = {uri: resp.uri, name: resp.fileName, type: resp.type};
                Qiscus.qiscus.upload(opts, (error, progress, fileURL) => {
                    if (error) {
                        this.setState({isFetching: false})
                        Alert.alert("Upload Image Gagal")
                    }
                    if (progress) {
                        console.log("Upload data proses : ", progress)
                    }
                    if (fileURL) {
                        let text = `Send Image`
                        let type = 'custom'
                        let now = moment.utc()
                        let payload = JSON.stringify({
                            type: 'image',
                            content: {
                                url: fileURL,
                                caption: "",
                                file_name: resp.fileName,
                            }
                        })
                        let _msg = {
                            "unique_temp_id": now.unix(),
                            "time": now,
                            "timestamp": now,
                            "type": type,
                            "email": this.props.data.account.email || "",
                            "status": "pending",
                            "message": text,
                            "payload": JSON.parse(payload)
                        }
                        this.setState({isFetching: false})
                        this.props.dispatch(updateMessage(this.getRoomId(), now.unix(), _msg))

                        Qiscus.qiscus.sendComment(this.getRoomId(), text, now.unix(), type, payload)
                            .then(comment => {
                                this.props.dispatch(updateMessage(this.getRoomId(), now.unix(), comment))
                            })
                            .catch(error => {
                                // On error
                                _msg.status = "failed"
                                this.props.dispatch(updateMessage(this.getRoomId(), now.unix(), _msg))
                                Alert.alert("Error", "Failed to send message")
                            })

                    }
                })
            }
        })
    }
    messages = () => {
        let message = this.props.data.messages[this.getRoomId()]
        if (message) {
            return this._sortMessage(Object.values(message));
        } else {
            return []
        }
    }
    _sortMessage = messages => messages.sort((a, b) => new Date(b.timestamp) - new Date(a.timestamp));
    room = val => {
        let room = this.props.data.rooms[this.getRoomId()]
        if (room) {
            return room[val]
        } else {
            Qiscus.qiscus.getRoomById(this.getRoomId()).then(data => {
                this.props.dispatch(updateRoom(this.getRoomId(), data))
            })
            return ""
        }
    }

    render() {
        const _data = this.messages()
        return (
            <View style={{backgroundColor: "#f7f7f7", flex: 1}}>
                <View style={style.header}>
                    <TouchableOpacity style={{alignItems: "center"}} onPress={() => {
                        this.exitRoom()
                        this.props.navigation.navigate("Home")
                    }}>
                        <Image source={require("../Assets/arrow_back_white.png")}
                               style={{width: 25, height: 25, marginRight: 20}}/>
                    </TouchableOpacity>
                    <Image source={{
                        uri: this.room("avatar") == "" ?
                            "https://d1edrlpyc25xu0.cloudfront.net/kiwari-prod/image/upload/75r6s_jOHa/1507541871-avatar-mine.png" :
                            this.room("avatar")
                    }} style={{width: 40, height: 40, borderRadius: 100}}/>
                    <View style={{marginLeft: 10}}>
                        <Text style={{fontWeight: "bold", color: "#ffffff"}}>{this.room("name")}</Text>
                        {this.state.isTyping &&
                        <Text style={{color: "#eee"}}>{this.room("name")} Sedang mengetik.....</Text>
                            /* : <Text style={{paddingTop: -0, color: "#1dee58"}}>{this.getOnline()}</Text>*/}
                    </View>
                </View>
                {this.state.isFetching &&
                <ActivityIndicator size={"large"} color="#371515" style={style.loading}/>}
                <FlatList
                    inverted={true}
                    style={{padding: 10, paddingTop: -5}}
                    keyExtractor={(item, index) => index.toString()}
                    data={Object.values(_data)}
                    renderItem={({item, index}) => {
                        return (
                            <View style={{paddingTop: 5, paddingBottom: 5}}>
                                {item.email == this.props.data.account.email ?
                                    <TouchableOpacity
                                        onPress={() =>
                                            item.status === "failed" && this.resendMsg(item)
                                        }
                                        onLongPress={() => this.deleteMsg(item)}>
                                        <View style={{alignSelf: 'flex-end'}}>
                                            <View style={{
                                                backgroundColor: '#eb180f',
                                                padding: 10,
                                                borderRadius: 20,
                                            }}>
                                                {item.type === "custom" ?
                                                    <View>
                                                        <Image source={{uri: item.payload.content.url}}
                                                               style={{height: 200, width: 200}}/>
                                                        <Text style={{
                                                            paddingTop: 5,
                                                            color: '#ffffff'
                                                        }}>{item.message}</Text>
                                                    </View>
                                                    :
                                                    <Text style={{
                                                        padding: 5,
                                                        color: '#ffffff'
                                                    }}>{item.message}</Text>
                                                }
                                                <View style={{
                                                    flexDirection: "row",
                                                    justifyContent: "flex-end"
                                                }}>{this.statusMsg(item.status)}</View>
                                            </View>
                                            <Text style={{
                                                marginLeft: 10,
                                                fontSize: 12
                                            }}>{moment(item.timestamp).calendar()}</Text>
                                        </View>
                                    </TouchableOpacity>
                                    :
                                    <View>
                                        <View style={{
                                            backgroundColor: '#ffffff',
                                            alignSelf: 'flex-start',
                                            padding: 10,
                                            borderRadius: 20
                                        }}>
                                            <Text style={{
                                                fontWeight: 'bold',
                                                color: '#ee0016'
                                            }}>{item.email}</Text>

                                            {item.type === "custom" ?
                                                <View>
                                                    <Image source={{uri: item.payload.content.url}}
                                                           style={{height: 200, width: 200}}/>
                                                    <Text style={{
                                                        paddingTop: 5,
                                                    }}>{item.message}</Text>
                                                </View>
                                                :
                                                <Text style={{
                                                    paddingTop: 5,
                                                }}>{item.message}</Text>
                                            }

                                        </View>
                                        <Text style={{
                                            marginLeft: 10,
                                            fontSize: 12
                                        }}>{moment(item.timestamp).calendar()}</Text>
                                    </View>
                                }
                            </View>
                        )
                    }}
                    onEndReached={() => {
                        this.loadMoreMsg()
                    }}
                    onEndReachedThreshold={0.7}
                />
                <View style={{backgroundColor: "white", padding: 15, flexDirection: "row", alignItems: "center"}}>
                    <TextInput
                        onBlur={() => {
                            this.userTypingDelay(0)
                            this.setState({onFocusText: false})
                        }}
                        value={this.state.message}
                        onChangeText={(message) => {
                            this.userTypingDelay(1)
                            this.setState({message})
                            this.setState({onFocusText: true})
                        }}
                        onSubmitEditing={() => {
                            this.sendMessage(this.state.message)
                        }}
                        placeholder={`Tulis Pesan ${this.state.onFocusText}`}
                        multiline={false}
                        style={style.inputMessage}
                    />

                    {this.cekTyping() === false && <View style={style.btnSend}>
                        <TouchableOpacity onPress={() => this.onChoosePhoto()}>
                            <Image source={require("../Assets/attachment.png")}
                                   style={{width: 25, height: 25, opacity: 100, transform: [{rotate: '70deg'}]}}/>
                        </TouchableOpacity>
                    </View>}
                    {this.cekTyping() && <View style={style.btnSend}>
                        <TouchableOpacity disabled={this.state.message === "" ? true : false}
                                          onPress={() => this.sendMessage(this.state.message)}>
                            <Image source={require("../Assets/send_white.png")}
                                   style={{width: 25, height: 25, opacity: 100}}/>
                        </TouchableOpacity>
                    </View>}
                </View>
            </View>
        );
    }
}

const mapsStateToProps = (state) => {
    return {
        data: state.data
    }
}
const style = {
    header: {
        flexDirection: "row", alignItems: "center", backgroundColor: "#eb180f", padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.22,
        shadowRadius: 2.22,
        elevation: 3,
        height: 60
    },
    loading: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 70,
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputMessage: {
        flex: 1,
        borderWidth: 0,
        borderRadius: 20,
        padding: 5,
        paddingLeft: 20,
        paddingRight: 20,
        backgroundColor: "#fafafa"
    },
    btnSend: {
        backgroundColor: "#eb180f",
        marginLeft: 10,
        padding: 10,
        borderRadius: 100,
        width: 40,
        height: 40,
        alignItems: "center",
        justifyContent: "center"
    }
}
export default connect(mapsStateToProps)(Chat)
