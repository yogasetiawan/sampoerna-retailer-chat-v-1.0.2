import React, {Component} from "react";
import {ActivityIndicator, Image, Text, View} from "react-native";
import {connect} from "react-redux";
import * as Qiscus from "../Services/qiscus"

class Splash extends Component {

    constructor(props) {
        super(props)
    }
    componentDidMount() {
        setTimeout(() => {
            this.props.data.account.token ?
                this.setDataUser() :
                this.props.navigation.replace("Login")
        }, 3000)
    }

    setDataUser() {
        const data = this.props.data.account
        Qiscus.qiscus.setUserWithIdentityToken({user: data})
        this.props.navigation.replace("Home")
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.logo}>
                    <Image source={require('../Assets/logo.png')} style={{height: 100, width: 100}}/>
                </View>
                <Text style={styles.text}>Qiscus Chat</Text>
                <ActivityIndicator size={'large'} color={"#eb180f"} style={{paddingTop: 20}}/>
            </View>
        )
    }
}

const mapsStateToProps = (state) => {
    return {
        data: state.data
    }
}
export default connect(mapsStateToProps)(Splash)
const styles = {
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    logo: {width: "100%", alignItems: 'center', marginTop: 100},
};
