import React, {Component} from "react"
import {
    ActivityIndicator,
    Image,
    Modal,
    ScrollView,
    Text,
    TextInput,
    ToastAndroid,
    TouchableOpacity,
    View
} from "react-native"
import * as Qiscus from "../Services/qiscus"
import {connect} from "react-redux"
import {setDataUser} from "../Services/Redux/action/Data";
import AsyncStorage from "@react-native-community/async-storage";
import firebase from "react-native-firebase";

class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            user: 'testRn',
            pass: 'testRn',
            loading: false
        }
    }

    onSubmit = () => {
        this.setState({loading: true})
        Qiscus.qiscus.setUser(this.state.user, this.state.pass)
            .then({})
            .catch(() => console.log("Failed login"));
    }

    async getToken() {
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        Qiscus.setDeviceToken(fcmToken)
        if (!fcmToken) {
            fcmToken = await firebase.messaging().getToken();
            if (fcmToken) {
                await AsyncStorage.setItem('fcmToken', fcmToken);
            }
        }
    }

    componentDidMount() {
        this.subscription = Qiscus.login$()
            .map(data => data.user)
            .subscribe({
                next: data => {
                    this.setState({loading: false})
                    this.getToken()
                    this.props.navigation.replace("Home")
                    this.props.dispatch(setDataUser(data))
                }
            });
        this.subscription2 = Qiscus.loginError$()
            .subscribe({
                next: data => {
                    this.setState({loading: false})
                    ToastAndroid.show("Login gagal", ToastAndroid.SHORT)
                }
            });

    }

    componentWillUnmount() {
        this.subscription && this.subscription.unsubscribe()
        this.subscription2 && this.subscription2.unsubscribe()
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <Modal
                    animationType="none"
                    visible={this.state.loading}
                    style={{flex: 1}}
                    transparent={true}
                    onRequestClose={() => this.setState({loading: false})}
                >
                    <ActivityIndicator
                        animating={true}
                        size="large"
                        color="#eb180f"
                        style={{
                            position: "absolute",
                            top: 0,
                            bottom: 0,
                            right: 0,
                            left: 0
                        }}/>
                </Modal>
                <ScrollView>
                    <View style={style.logo}>
                        <Image source={require('../Assets/logo.png')} style={{height: 100, width: 100}}/>
                    </View>
                    <View style={style.body}>
                        <Text style={style.text}>USER ID</Text>
                        <TextInput
                            placeholder={"nput user id"}
                            style={style.textInput}
                            onChangeText={(user) => this.setState({user})}
                            defaultValue={this.state.user}
                        />
                        <Text style={style.text}>USER KEY</Text>
                        <TextInput
                            placeholder={"Input user key"}
                            style={style.textInput}
                            defaultValue={this.state.pass}
                            secureTextEntry={true}
                            onChangeText={(pass) => this.setState({pass})}/>
                        <View style={style.btnWrapper}>
                            <TouchableOpacity style={{flex: 1, margin: 5}} onPress={() => this.onSubmit()}>
                                <View style={style.btn}>
                                    <Text style={style.btnText}>Login</Text>
                                </View>
                            </TouchableOpacity>

                            <TouchableOpacity style={{flex: 1, margin: 5}}
                                              onPress={() => this.props.navigation.navigate("Register")}>
                                <View style={style.btn}>
                                    <Text style={style.btnText}>Register</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const mapsStateToProps = (state) => {
    return {
        data: state.data
    }
}
export default connect(mapsStateToProps)(Login)
const style = {
    logo: {width: "100%", alignItems: 'center', marginTop: 100},
    body: {marginTop: 90, paddingLeft: 50, paddingRight: 50},
    text: {fontWeight: 'bold', marginTop: 10, color: '#979797'},
    textInput: {borderBottomWidth: 1, borderBottomColor: '#eb180f', paddingLeft: 15},
    btn: {
        backgroundColor: '#dc1e2d',
        padding: 10,
        borderRadius: 5,
        marginTop: 40,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnText: {color: 'white', fontWeight: 'bold', fontSize: 20},
    btnIcon: {width: 25, height: 25},
    btnWrapper: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        alignContent: 'center'
    }
};
