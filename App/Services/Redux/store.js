import {applyMiddleware, createStore} from 'redux';
import {createLogger} from "redux-logger";
import promiseMiddleware from 'redux-promise-middleware';
import reducers from './reducers';
import AsyncStorage from '@react-native-community/async-storage';
import {persistCombineReducers, persistStore} from "redux-persist";
import {composeWithDevTools} from "redux-devtools-extension"

const config = {
    key: 'primary',
    storage:AsyncStorage
};


const logger = createLogger({});

let persistedReducer = persistCombineReducers(config, reducers);

export default () => {
    const store = createStore(persistedReducer, composeWithDevTools
        (
        applyMiddleware(
           // logger,
            promiseMiddleware,
        )
        )
    );
    let persistor = persistStore(store);
    return {
        store,
        persistor
    };
};