const initialState = {
    account: {},
    messages: {},
    curretRoom: {},
    rooms: {},
    lastUpdateRoom: 0
}

export default data = (state = initialState, action) => {
    switch (action.type) {
        //SET USER ID and TOKEN
        case 'SET_USER' : {
            return {
                ...state,
                account: action.payload,
            }
        }
        case 'SET_CURRET_ROOM' : {
            return {
                ...state,
                curretRoom: action.payload,
            }
        }
        case 'SET_ROOM': {
            return {
                ...state,
                rooms: action.payload
            }
        }
        case 'SET_LAST_UPDATE_ROOM': {
            return {
                ...state,
                lastUpdateRoom: action.payload
            }
        }
        case 'UPDATE_ROOM': {
            return {
                ...state,
                rooms: {
                    ...state.rooms,
                    [action.roomId]: action.data
                }
            }
        }

        case 'SET_MESSAGES' : {
            //let data =
            return {
                ...state,
                messages: {
                    ...state.messages,
                    [action.roomId]: action.data
                },

            }
        }

        case 'UPDATE_MESSAGES' : {
            //let data =
            return {
                ...state,
                messages: {
                    ...state.messages,
                    [action.roomId]: {
                        ...state.messages[action.roomId],
                        ...action.data
                    },
                },

            }
        }
        case 'UPDATE_MESSAGE' : {
            return {
                ...state,
                messages: {
                    ...state.messages,
                    [action.roomId]: {
                        ...state.messages[action.roomId],
                        [action.unique_temp_id]: action.message
                    },
                },

            }
        }
        case 'DELETED_MESSAGE' : {
            let msg = state.messages[action.roomId]
            if (Object.entries(msg).length !== 0) {
                let newMsg = Object.values(msg).filter(a => action.arrayId.indexOf(a.unique_temp_id) === -1)
                let messages = newMsg.reduce((result, item) => {
                    let uniqueId = item.unique_id || item.unique_temp_id
                    result[uniqueId] = item
                    return result
                }, {})
                return {
                    ...state,
                    messages: {
                        ...state.messages,
                        [action.roomId]: messages
                    }
                }
            } else {
                return state
            }
        }
        default:
            return state;
    }
}
