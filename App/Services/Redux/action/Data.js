export const setDataUser = (data) => {
    return {
        type:'SET_USER',
        payload: data
    }
}

export const setCurretRoom = (room) => {
    return {
        type: 'SET_CURRET_ROOM',
        payload: room
    }
}

export const setRooms = (rooms)=>{
    return{
        type:'SET_ROOM',
        payload: rooms
    }
}
export const setLastUpdateRoom = (time)=>{
    return{
        type:'SET_LAST_UPDATE_ROOM',
        payload: time
    }
}
export const updateRoom = (roomId, data)=>{
    return{
        type:'UPDATE_ROOM',
        roomId,
        data
    }
}
export const setMessages = (roomId,data) => {
    return {
        type:'SET_MESSAGES',
        roomId,
        data
    }
}
export const updateMessages = (roomId,data) => {
    return {
        type:'UPDATE_MESSAGES',
        roomId,
        data
    }
}
export const updateMessage = (roomId,unique_temp_id, message) => {
    return {
        type:'UPDATE_MESSAGE',
        roomId,
        unique_temp_id,
        message
    }
}

export const deletedMessage = (roomId,arrayId) => {
    return {
        type:'DELETED_MESSAGE',
        roomId,
        arrayId
    }
}

