import React, {Component} from 'react'
import AppNaigator from './Navigations/AppNavigator'
import * as Qiscus from './Services/qiscus'
import {Provider} from "react-redux"
import configStore from "./Services/Redux/store"
import {PersistGate} from "redux-persist/integration/react";
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';

const {persistor, store} = configStore();

export default class App extends Component {
    componentDidMount() {
        Qiscus.init()
        const channel = new firebase.notifications.Android.Channel(
            'insider',
            'insider channel',
            firebase.notifications.Android.Importance.Max)
        firebase.notifications().android.createChannel(channel);

        this.checkPermission();
        this.createNotificationListeners();
    }

    async getToken() {
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        if (!fcmToken) {
            fcmToken = await firebase.messaging().getToken();
            if (fcmToken) {
                await AsyncStorage.setItem('fcmToken', fcmToken);
            }
        }
    }

    async checkPermission() {
        const enabled = await firebase.messaging().hasPermission();
        if (enabled) {
            this.getToken();
        } else {
            this.requestPermission();
        }
    }

    async requestPermission() {
        try {
            await firebase.messaging().requestPermission();
            this.getToken();
        } catch (error) {
            console.log('permission rejected');
        }
    }

    async createNotificationListeners() {
        firebase.notifications().onNotification(notification => {
            notification.android.setChannelId('insider').setSound('default')
            let {curretRoom} = store.getState().data
            if (notification.data.qiscus_room_id == curretRoom.id) {
                if (typeof notification.data.qiscus_room_id !== undefined) {
                    firebase.notifications().displayNotification(notification)
                    //console.log("tampilan notif", notification)
                } else {
                    //console.log("Hidden notif")
                }
            } else {
                firebase.notifications().displayNotification(notification)
                //console.log("tampilan notif", notification)
            }

        });
    }
    render() {
        return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <AppNaigator/>
                </PersistGate>
            </Provider>
        )
    }
}
