import Splash from '../Screens/Splash'
import {createAppContainer, createStackNavigator} from "react-navigation"
import Login from "../Screens/Login";
import Home from "../Screens/Home";
import Chat from "../Screens/Chat";
import Register from "../Screens/Register";
import NewRoom from "../Screens/NewRoom";
import Qismo from "../Screens/Qismo";


const AppNavigator = createStackNavigator({

    Splash:{
        screen:Splash,
        navigationOptions: {
            header: null //this will hide the header
        }
    },
    NewRoom:{
        screen:NewRoom,
        navigationOptions: {
            header: null //this will hide the header
        }
    },
     Login:{
        screen:Login,
         navigationOptions: {
             header: null //this will hide the header
         }
     },
    Register:{
        screen:Register,
        navigationOptions: {
            header: null //this will hide the header
        }
    },
    Chat: {
        screen: Chat,
        navigationOptions: {
            title: "Chat Room",
        }
    },
    Home: {
        screen: Home,
        navigationOptions: {
            title: "Home",
        }
    },
    Qismo: {
        screen: Qismo,
        navigationOptions: {
           header:null
        }
    }
});

export default createAppContainer(AppNavigator)