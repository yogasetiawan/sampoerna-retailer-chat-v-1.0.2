#RN-ayosrc
## How to run

```bash

Install react-native-cli from npm globally
$ npm install react-native-cli -g

# Choose folder and install the dependency
$ cd rn-ayosrc
$ npm install

# Run project with desired platform if you prefer use Android run code below
$ react-native run-android

# Run project with desired platform if you prefer use IOS run code below
$ react-native run-ios
```
